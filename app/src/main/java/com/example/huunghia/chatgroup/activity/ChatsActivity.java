package com.example.huunghia.chatgroup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.adapter.MessagesAdapter;
import com.example.huunghia.chatgroup.model.Message;
import com.example.huunghia.chatgroup.storage.SharedPrefManager;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.UUID;

public class ChatsActivity extends AppCompatActivity {

    private DatabaseReference chatsRef, rootRef, messagesRef;

    private ArrayList<String> arrMember, tmp;

    private String emailCurrentUser, email, key;

    private Toolbar toolbar;
    private ImageButton send, add;
    private EditText editText;

    private ListView lvMessage;
    private MessagesAdapter adapter;
    private ArrayList<Message> messages;

    private Message message;

    private Uri filePath, downUri;

    private final int PICK_IMAGE_REQUEST = 71;

    //Firebase
    private FirebaseStorage storage;
    private StorageReference storageReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        send = (ImageButton) findViewById(R.id.send);
        add  = (ImageButton) findViewById(R.id.add);

        editText = (EditText) findViewById(R.id.editText);

        toolbar = (Toolbar) findViewById(R.id.chats_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Chats");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rootRef = FirebaseDatabase.getInstance().getReference();
        chatsRef = rootRef.child("Chats");

        arrMember = new ArrayList<>(2);
        tmp = new ArrayList<>(2);

        if (MainActivity.currentUser != null) {

            emailCurrentUser = MainActivity.currentUser.getEmail();
            email = getIntent().getStringExtra("email");
            key = SharedPrefManager.getInstance(this).getKey();
        }


        message = new Message();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String content = editText.getText().toString();

                if (TextUtils.isEmpty(content)){
                    Toast.makeText(ChatsActivity.this, "nhap tin nhan", Toast.LENGTH_SHORT).show();
                }else {

                    message = new Message(emailCurrentUser, content);

                    String randomKey = rootRef.push().getKey();

                    chatsRef.child(key).child("messages").child(randomKey).setValue(message);

                    editText.setText("");
                }

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });


        messages = new ArrayList<>();

        lvMessage = findViewById(R.id.lvMessage);
        adapter = new MessagesAdapter(ChatsActivity.this, R.layout.layout_messages_adapter, messages);
        lvMessage.setAdapter(adapter);


        if (key != null){
            messagesRef = chatsRef.child(key).child("messages");
            messagesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    ArrayList<Message> tmp = new ArrayList<>();

                    for (DataSnapshot i : dataSnapshot.getChildren()){

                        Message messageObj = i.getValue(Message.class);
                        tmp.add(messageObj);
                    }

                    messages.clear();
                    messages.addAll(tmp);
                    adapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();

            uploadImage();

        }
    }

    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Sending...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());

            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){

                        progressDialog.dismiss();

                        downUri = task.getResult();

                        message = new Message();

                        message.setImageUri(downUri.toString());
                        message.setFrom(emailCurrentUser);

                        String randomKey = rootRef.push().getKey();

                        chatsRef.child(key).child("messages").child(randomKey).setValue(message);

                    }
                }
            });


        }

    }

    private void addNewChats(String chatsName) {

        arrMember.clear();
        arrMember.add(email);
        arrMember.add(emailCurrentUser);

        chatsRef.child(chatsName).child("members").setValue(arrMember);
    }

}
