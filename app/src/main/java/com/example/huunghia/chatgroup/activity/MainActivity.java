package com.example.huunghia.chatgroup.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.adapter.TabsAccessorAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabsAccessorAdapter tabsAccessorAdapter;
    
    public static FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;

    private String emailCurrentUser;
    private ArrayList<String> members;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        members = new ArrayList<>();

        toolbar = (Toolbar) findViewById(R.id.main_page_toolar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ChatGroup");

        viewPager = (ViewPager) findViewById(R.id.main_pager);
        tabsAccessorAdapter = new TabsAccessorAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsAccessorAdapter);

        tabLayout = (TabLayout) findViewById(R.id.main_tab);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(2);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        rootRef = FirebaseDatabase.getInstance().getReference();

        if (currentUser != null){
            emailCurrentUser = currentUser.getEmail();
            members.clear();
            members.add(emailCurrentUser);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        
        if (currentUser == null){
            sendToLoginActivity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.main_log_out){

            mAuth.signOut();
            sendToLoginActivity();
        }

        if (item.getItemId() == R.id.main_create_group){

            createGroup();
        }


        return super.onOptionsItemSelected(item);
    }

    private void createGroup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Create new group");

        final EditText edtGroupName = new EditText(MainActivity.this);
        edtGroupName.setHint("Nhap ten group");
        builder.setView(edtGroupName);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String groupName = edtGroupName.getText().toString().trim();

                if (TextUtils.isEmpty(groupName)){
                    Toast.makeText(MainActivity.this, "nhap ten group", Toast.LENGTH_SHORT).show();
                } else addNewGroup(groupName);

            }
        });

        builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.show();

    }

    private void addNewGroup(String groupName) {

        rootRef.child("Groups").child(groupName).child("members").setValue(members);
    }

    private void sendToLoginActivity() {

        Intent loginIntent= new Intent(MainActivity.this, LoginActivity.class);
        startActivity(loginIntent);
    }
}
