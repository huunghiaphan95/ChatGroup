package com.example.huunghia.chatgroup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.adapter.MessagesAdapter;
import com.example.huunghia.chatgroup.model.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChatsGroupActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton add, send;
    private EditText editText;

    private ListView lvMessage;
    private MessagesAdapter adapter;
    private ArrayList<Message> messages;

    private Message message;

    private FirebaseUser currentUser;
    private DatabaseReference groupRef, messagesRef;

    public static String currentGroupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats_group);

        currentGroupName = getIntent().getStringExtra("group_name");

        toolbar = (Toolbar) findViewById(R.id.chats_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(currentGroupName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        groupRef = FirebaseDatabase.getInstance().getReference().child("Groups");

        final String emailCurrentUser = currentUser.getEmail();

        add  = (ImageButton) findViewById(R.id.add);
        send = (ImageButton) findViewById(R.id.send);

        editText = (EditText) findViewById(R.id.editText);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String content = editText.getText().toString();

                if (TextUtils.isEmpty(content)){
                    Toast.makeText(ChatsGroupActivity.this, "nhap tin nhan", Toast.LENGTH_SHORT).show();
                }else {

                    message = new Message(emailCurrentUser, content);

                    String randomKey = groupRef.push().getKey();

                    groupRef.child(currentGroupName).child("messages").child(randomKey).setValue(message);

                    editText.setText("");
                }

            }
        });

        messages = new ArrayList<>();

        lvMessage = findViewById(R.id.lvMessage);
        adapter = new MessagesAdapter(ChatsGroupActivity.this, R.layout.layout_messages_adapter, messages);
        lvMessage.setAdapter(adapter);

        messagesRef = groupRef.child(currentGroupName).child("messages");
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<Message> tmp = new ArrayList<>();

                for (DataSnapshot i : dataSnapshot.getChildren()){

                    Message messageObj = i.getValue(Message.class);
                    tmp.add(messageObj);
                }

                messages.clear();
                messages.addAll(tmp);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent usersIntent = new Intent(ChatsGroupActivity.this, UsersActivity.class);
                startActivity(usersIntent);
            }
        });
    }
}
