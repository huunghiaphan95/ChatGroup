package com.example.huunghia.chatgroup.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.model.Message;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MessagesAdapter extends ArrayAdapter<Message>{

    private Context context;
    private int resource;
    private ArrayList<Message> items;

    class ViewHolder {
        private TextView from;
        private TextView to;
        private ImageView imageView;
    }

    public MessagesAdapter(Context context, int resource, ArrayList<Message> objects) {
        super(context, resource, objects);
        this.context    = context;
        this.resource   = resource;
        this.items      = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder;

        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view    = inflater.inflate(resource, parent, false);

            viewHolder  = new ViewHolder();
            viewHolder.from         = (TextView) view.findViewById(R.id.from);
            viewHolder.to           = (TextView) view.findViewById(R.id.to);
            viewHolder.imageView    = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(viewHolder);
        }else{
            viewHolder  = (ViewHolder) view.getTag();
        }

        Message messageCurrent = items.get(position);

        if(messageCurrent != null) {
            viewHolder.from.setText(messageCurrent.getFrom());

            if (messageCurrent.getContent() == null){
                viewHolder.to.setText("");
            }else {
                viewHolder.to.setText(messageCurrent.getContent());
            }

            Picasso.get()
                    .load(messageCurrent.getImageUri())
                    .into(viewHolder.imageView);
        }

        return view;
    }
}
