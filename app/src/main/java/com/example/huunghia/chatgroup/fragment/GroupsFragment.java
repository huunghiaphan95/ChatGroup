package com.example.huunghia.chatgroup.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.activity.ChatsGroupActivity;
import com.example.huunghia.chatgroup.activity.MainActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class GroupsFragment extends Fragment {

    private ListView lvGroup;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> groups = new ArrayList<>();
    private ArrayList<String> emails = new ArrayList<>();

    private View view;

    private DatabaseReference rootRef, groupsRef;

    private String emailCurrentUser;

    public GroupsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_groups, container, false);

        setupListView();

        rootRef = FirebaseDatabase.getInstance().getReference();

        if (MainActivity.currentUser != null) {

            emailCurrentUser = MainActivity.currentUser.getEmail();
            loadGroups();
        }



        lvGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String groupName = (String) lvGroup.getItemAtPosition(i);

                Intent chatsIntent = new Intent(getContext(), ChatsGroupActivity.class);
                chatsIntent.putExtra("group_name", groupName);
                startActivity(chatsIntent);
            }
        });

        return view;
    }


    private void loadGroups() {

        groupsRef = rootRef.child("Groups");

        groupsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<String> set = new ArrayList<>();

                for (DataSnapshot currentSnapshot : dataSnapshot.getChildren()){

                   emails = (ArrayList<String>) currentSnapshot.child("members").getValue();

                   for (int i = 0; i < emails.size(); i++){
                       if (emailCurrentUser.equals(emails.get(i))){
                           set.add(currentSnapshot.getKey());
                       }
                   }
                }

                groups.clear();
                groups.addAll(set);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setupListView() {

        lvGroup = (ListView) view.findViewById(R.id.lvGroup);
        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, groups);
        lvGroup.setAdapter(adapter);
    }


}
