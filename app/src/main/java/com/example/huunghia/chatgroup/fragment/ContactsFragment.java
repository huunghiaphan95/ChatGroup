package com.example.huunghia.chatgroup.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.huunghia.chatgroup.R;
import com.example.huunghia.chatgroup.storage.SharedPrefManager;
import com.example.huunghia.chatgroup.activity.ChatsActivity;
import com.example.huunghia.chatgroup.activity.MainActivity;
import com.example.huunghia.chatgroup.model.Chat;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    private View view;

    private ListView lvUser;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrUser = new ArrayList<>();

    private ArrayList<String> arrMember = new ArrayList<>();
    private ArrayList<String> tmp = new ArrayList<>();

    private DatabaseReference usersRef, rootRef, chatsRef;

    private String emailCurrentUser;

    private String uid, email, key;


    public ContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        setupListView();

        if (MainActivity.currentUser != null) {

            emailCurrentUser = MainActivity.currentUser.getEmail();

            uid = MainActivity.currentUser.getUid();

        }

        rootRef = FirebaseDatabase.getInstance().getReference();
        usersRef = rootRef.child("Users");
        chatsRef = rootRef.child("Chats");

        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<String> tmp = new ArrayList<>();

                for (DataSnapshot i : dataSnapshot.getChildren()){

                    if (!i.getKey().equals(uid)){

                        tmp.add(i.getValue().toString());
                    }
                }

                arrUser.clear();
                arrUser.addAll(tmp);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                email = (String) lvUser.getItemAtPosition(i);

                chatsRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        boolean k = false;

                        for (DataSnapshot currentSnapshot : dataSnapshot.getChildren()) {

                            Chat chatObj = currentSnapshot.getValue(Chat.class);

                            tmp = chatObj.getMembers();

                            if ((emailCurrentUser.equals(tmp.get(0)) || emailCurrentUser.equals(tmp.get(1)))
                                    && (email.equals(tmp.get(0)) || email.equals(tmp.get(1)))) {

                                k = true;

                                key = currentSnapshot.getKey();

                                SharedPrefManager.getInstance(getActivity()).saveKey(key);

                            }

                        }

                        if (k == false) {
                            addNewChats(rootRef.push().getKey());
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });

                Intent chatsIntent = new Intent(getActivity(), ChatsActivity.class);
                chatsIntent.putExtra("email", email);
                startActivity(chatsIntent);

            }
        });

        
        return view;
    }

    private void addNewChats(String chatsName) {

        arrMember.clear();
        arrMember.add(email);
        arrMember.add(emailCurrentUser);

        chatsRef.child(chatsName).child("members").setValue(arrMember);
    }

    private void setupListView() {

        lvUser = (ListView) view.findViewById(R.id.lvUser);
        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, arrUser);
        lvUser.setAdapter(adapter);
    }



}
