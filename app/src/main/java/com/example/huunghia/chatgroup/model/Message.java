package com.example.huunghia.chatgroup.model;

public class Message {

    private String from;
    private String content;
    private String imageUri;

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public Message(String from, String content) {
        this.from = from;
        this.content = content;
    }

    public Message() {
    }

    @Override
    public String toString() {
        return "from: " + from + "\n"
                + content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
