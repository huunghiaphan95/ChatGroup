package com.example.huunghia.chatgroup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.huunghia.chatgroup.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText edtEmail, edtPassword;
    private Button btnRegister;

    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;

    private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        anhXa();

        //cai dat toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dang ky");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

        loadingBar = new ProgressDialog(this);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                creatNewAccount();

            }
        });
    }

    private void creatNewAccount() {

        final String email    = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            Toast.makeText(RegisterActivity.this, "nhap email", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(password)) {
            Toast.makeText(RegisterActivity.this, "nhap password", Toast.LENGTH_SHORT).show();
        }else {

            loadingBar.setTitle("Dang ky tai khoan");
            loadingBar.setMessage("Doi trong giay lat");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()){

                                String userUid = mAuth.getCurrentUser().getUid();

                                rootRef.child("Users").child(userUid).setValue(email);

                                loadingBar.dismiss();

                                sendToMainActivity();

                                Toast.makeText(RegisterActivity.this, "dang ky thanh cong", Toast.LENGTH_SHORT).show();
                            }else {

                                loadingBar.dismiss();

                                Toast.makeText(RegisterActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }

    private void sendToLoginActivity() {

        Intent loginIntent= new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(loginIntent);
    }

    private void sendToMainActivity() {

        Intent mainIntent= new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(mainIntent);
    }

    private void anhXa() {

        toolbar = (Toolbar) findViewById(R.id.register_toolbar);

        edtEmail    = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnRegister = (Button) findViewById(R.id.btnRegister);
    }
}
