package com.example.huunghia.chatgroup.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.huunghia.chatgroup.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ListView lvUser;

    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrUser;

    private FirebaseUser currentUser;
    private DatabaseReference usersRef, groupRef;

    private ArrayList<String> arrMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        final String currentGroupName = ChatsGroupActivity.currentGroupName;

        toolbar = findViewById(R.id.users_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Users");

        arrUser = new ArrayList<>();
        arrMember = new ArrayList<>();


        lvUser = findViewById(R.id.lvUser);
        adapter = new ArrayAdapter<>(UsersActivity.this, android.R.layout.simple_list_item_1, arrUser);
        lvUser.setAdapter(adapter);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        groupRef = FirebaseDatabase.getInstance().getReference().child("Groups");

        final String uid = currentUser.getUid();

        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<String> tmp = new ArrayList<>();

                for (DataSnapshot i : dataSnapshot.getChildren()){

                    if (!i.getKey().equals(uid)){

                        tmp.add(i.getValue().toString());
                    }
                }

                arrUser.clear();
                arrUser.addAll(tmp);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        groupRef.child(currentGroupName).child("members").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                arrMember.clear();
                arrMember.addAll((ArrayList<String>) dataSnapshot.getValue());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final String email = (String) lvUser.getItemAtPosition(i);

                AlertDialog.Builder builder = new AlertDialog.Builder(UsersActivity.this);
                builder.setTitle("Them vao nhom");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        boolean flag = false;

                        for (int j = 0; j < arrMember.size(); j++){

                            if (arrMember.get(j).equals(email)) {
                                flag = true;
                                break;
                            }
                        }

                        if (flag == false) arrMember.add(email);

                        groupRef.child(currentGroupName).child("members").setValue(arrMember);
                        finish();


                    }
                });

                builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                builder.show();
            }
        });


    }
}
