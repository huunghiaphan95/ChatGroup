package com.example.huunghia.chatgroup.model;

import java.util.ArrayList;

public class Chat {

    private boolean group;
    private ArrayList<String> members;
    private Message message;

    public Chat() {
    }

    public Chat(boolean group, ArrayList<String> members, Message message) {
        this.group = group;
        this.members = members;
        this.message = message;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public ArrayList<String> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<String> members) {
        this.members = members;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
